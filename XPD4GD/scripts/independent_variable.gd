class_name IndependentVariable

extends Resource

enum PresentationMode {IN_SEQUENCE, RANDOMIZED}

@export var variable_name: String = "variable name"
@export var presentation_mode:PresentationMode = PresentationMode.IN_SEQUENCE
@export var values = []
