class_name ConstantStimuliProcedure

extends Resource

@export var nb_repetitions_per_condition:=1
@export var independent_variables:Array[IndependentVariable]
