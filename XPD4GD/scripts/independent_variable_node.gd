@tool
class_name IndependentVariableNode
extends Node

@export var variable_name: String = "variable name"

@export var values = []

#@EditorProperty(hint="List of values", usage="") var customList = []
#
#func _get_property_list():
#	var properties = []
#	if dataType == DataType.COLOR:
#		for i in range(customList.size()):
#			properties.append(PropertyInfo(Variant.COLOR, "customList", PROPERTY_HINT_NONE, "", i))
#	elif dataType == DataType.VECTOR3:
#		for i in range(customList.size()):
#			properties.append(PropertyInfo(Variant.VECTOR3, "customList", PROPERTY_HINT_NONE, "", i))
#	return properties
#
#func _set(property: String, value: Variant):
#	if property == "customList":
#		if dataType == DataType.COLOR:
#			customList.append(value)
#		elif dataType == DataType.VECTOR3:
#			customList.append(value)
#	else:
#		return set(property, value)
