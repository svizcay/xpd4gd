@tool
extends Node3D

@export var eccentricity: float = 0.0 :
	set (val):
		var result = Vector3(0, 0, 0)
		eccentricity = val
		result.x = radial_distance * sin((PI * 2) / 360. * eccentricity) * cos((PI * 2) / 360. * half_meridian)
		result.y = radial_distance * sin((PI * 2) / 360. * eccentricity) * sin((PI * 2) / 360. * half_meridian)
		result.z = radial_distance * cos((PI * 2) / 360. * eccentricity)
		position = result
		#print(position)
		#notify_property_list_changed()
	get:
		return eccentricity
		
@export var radial_distance: float = 0.01 :
	set (val):
		radial_distance = val
		position = Vector3(val, 0, 0)
		#print(position)
		#notify_property_list_changed()
	get:
		return radial_distance
		
@export var half_meridian: float = 0.0 :
	set (val):
		half_meridian = val
		position = Vector3(val, 0, 0)
		#print(position)
		#notify_property_list_changed()
	get:
		return half_meridian
		

#@export var x_pos: float = 0.0 : set = update_position
#@export var y_pos: float = 0.0 : set = update_position
#@export var z_pos: float = 0.0 : set = update_position
##var y_pos: float = 0.0 setget set_position
#var z_pos: float = 0.0 setget set_position

func update_position(value: float) -> void:
	print("value updated to %d" % value)
	#var position = Vector3(x_pos, y_pos, z_pos)
	#set_position(position)

func _process(delta):
	pass
	#rotation += PI * delta
	#print("delta %f" % delta)
